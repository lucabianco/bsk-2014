﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace crypto
{
    class Rail
    {
        /**
         * klasa szyfrujaca, gdzie rail to klucz a plainText to tekst wprowadzony
         */ 

        public static string Encrypt(int rail, string plainText)
        {
            /**
             * lista w ktorej przechowywany jest tekst
             */ 

            List<string> railFence = new List<string>();
            for (int i = 0; i < rail; i++)
            {
                railFence.Add("");
            }

            int number = 0;
            int increment = 1;
            foreach (char c in plainText) /** dla kazdego znaku tekstu */
            {
                if (number + increment == rail) /** sprawdzamy czy 0 + 1 jest = kluczowi, jezeli tak to -1*/
                {
                    increment = -1;
                }
                else if (number + increment == -1) /** sprawdzamy czy 0 + 1 jest = -1, jezeli tak to 1*/
                {
                    increment = 1;
                }
                railFence[number] += c; /** railfence[number] = railfence[number] + c */
                number += increment; /** number = number + increment */
            }

            /**
             * pozwala nam to na wyswietlanie tego co sie wydarzylo wczesniej
             */ 

            string buffer = "";
            foreach (string s in railFence)
            {
                buffer += s;
            }
            return buffer;
        }
        /** klasa odszyfrowujaca, gdzie rail to klucz a cipherText to tekst zaszyfrowany */
        public static string Decrypt(int rail, string cipherText)
        {
            /** pobieramy dlugosc tekstu zaszyfrowanego */
            int cipherLength = cipherText.Length;
            List<List<int>> railFence = new List<List<int>>();
            for (int i = 0; i < rail; i++)
            {
                railFence.Add(new List<int>());
            }

            int number = 0;
            int increment = 1;
            for (int i = 0; i < cipherLength; i++) /** dla kazdego znaku tekstu zaszyfrowanego */
            {
                if (number + increment == rail)
                {
                    increment = -1;
                }
                else if (number + increment == -1)
                {
                    increment = 1;
                }
                railFence[number].Add(i);
                number += increment;
            }

            int counter = 0;
            char[] buffer = new char[cipherLength];
            for (int i = 0; i < rail; i++)
            {
                for (int j = 0; j < railFence[i].Count; j++)
                {
                    buffer[railFence[i][j]] = cipherText[counter];
                    counter++;
                }
            }

            return new string(buffer);
        }
    }
}