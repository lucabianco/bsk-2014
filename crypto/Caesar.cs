﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace crypto
{
    /**
     * klasa z szyfrem Cezara 
     */
    public static class Caesar
    {
        /**
         * dwa do potegi szesnastej, daje obsluge wszystkich istniejacych znakow
         */ 
        private static int ile = 65536;

        /**
         * String szyfrowania, pobiera tekst i uzywa zadanego klucza
         */ 
        public static String Encrypt(String text, int k)
        {
            StringBuilder sb = new StringBuilder();

            /**
             * dla kazdego znaku przeprowadzone jest szyfrowanie, gdzie c to znak a k klucz modulo ile
             */ 
            foreach (char c in text)
                sb.Append(Convert.ToChar((c + k) % ile));

            return sb.ToString();
        }

        public static String Decrypt(String text, int k)
        {

            StringBuilder sb = new StringBuilder();

            /**
             * dla kazdego znaku przeprowadzone jest odszyfrowanie, gdzie c to znak a k klucz, ile - k i modulo ile
             */ 
            foreach (char c in text)
                sb.Append(Convert.ToChar((c + (ile - k)) % ile));

            return sb.ToString();
        }
    }
}